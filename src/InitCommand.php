<?php

namespace Avris\Deployer;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'init',
    description: 'Initialise the deployer configuration',
)]
class InitCommand extends Command
{
    public function __construct(private Deployer $deployer)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addArgument('repo', InputArgument::REQUIRED, 'URL of the GIT repository');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $projectDir = $this->deployer->init($input->getArgument('repo'));

        $output->writeln(sprintf('Initialised a deployer configuration in <info>%s</info>', $projectDir));
        $output->writeln('');

        $output->writeln('Please modify <info>deploy.php</info> and <info>shared</info> to match your needs.');
        $output->writeln('');

        $output->writeln(sprintf('Please make sure your project (<info>%s</info>) has a <info>Makefile</info> with a <info>deploy</info> directive. For example:', $input->getArgument('repo')));
        $output->writeln(<<<'TEXT'
    deploy:
        composer install --no-dev --optimize-autoloader
        bin/console doctrine:migration:migrate -n
        yarn
        yarn build
TEXT
        );
        $output->writeln('');

        $output->write("\x07");

        return Command::SUCCESS;
    }
}
