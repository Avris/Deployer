<?php

namespace Avris\Deployer;

final class GitLabArtifactsArchiveDownloadFetchMode implements FetchMode
{
    private string $gitLabInstance;
    private int $projectId;
    private string $jobName;
    private string $privateToken;

    public function __construct(string $gitLabInstance, int $projectId, string $jobName, string $privateToken)
    {
        $this->gitLabInstance = $gitLabInstance;
        $this->projectId = $projectId;
        $this->jobName = $jobName;
        $this->privateToken = $privateToken;
    }

    public function fetch(Config $config, string $branch, string $releaseDir): iterable
    {
        $projectUrl = $this->gitLabInstance . '/api/v4/projects/' . $this->projectId;

        $curl = curl_init($projectUrl . '/repository/commits/' . $branch);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = json_decode(curl_exec($curl));
        curl_close($curl);
        $pipelineId = $response->last_pipeline->id;

        $curl = curl_init($projectUrl . '/pipelines/' . $pipelineId . '/jobs?per_page=100');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = json_decode(curl_exec($curl));
        curl_close($curl);

        $jobId = null;
        foreach ($response as $job) {
            if ($job->name === $this->jobName) {
                if ($job->status !== 'success') {
                    throw new \Exception('job ' . $this->jobName . ' has status ' . $job->status);
                }
                $jobId = $job->id;
                break;
            }
        }

        if ($jobId === null) {
            throw new \Exception('job ' . $this->jobName . ' not found');
        }

        return [
            [
                'curl', '--location', '--fail', '--header', 'PRIVATE-TOKEN: ' . $this->privateToken,
                $projectUrl . '/jobs/' . $jobId . '/artifacts', '--output', $releaseDir . '/artifacts.zip',
            ],
            ['unzip', $releaseDir . '/artifacts.zip', '-d', $releaseDir],
            ['rm', $releaseDir . '/artifacts.zip'],
        ];
    }
}
