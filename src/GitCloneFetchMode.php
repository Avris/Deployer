<?php

namespace Avris\Deployer;

final class GitCloneFetchMode implements FetchMode
{
    public function fetch(Config $config, string $branch, string $releaseDir): iterable
    {
        return [
            ['git', 'clone', $config->repositoryUrl(), $releaseDir],
            ['git', 'checkout', $branch],
        ];
    }
}
