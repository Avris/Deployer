<?php

namespace Avris\Deployer;

abstract class Config
{
    public function fetchMode(): FetchMode
    {
        return new GitCloneFetchMode();
    }

    abstract public function repositoryUrl(): string;

    public function branch(): string
    {
        return 'main';
    }

    public function keepReleases(): int
    {
        return 3;
    }

    /**
     * @return iterable<string>
     */
    public function sharedFiles(): iterable
    {
        return [];
    }

    /**
     * @return iterable<string>
     */
    public function removeFiles(): iterable
    {
        yield '.git';
    }

    /**
     * @return iterable<string[]>
     */
    public function before(): iterable
    {
        return [];
    }

    /**
     * @return iterable<string[]>
     */
    public function build(): iterable
    {
        yield ['make', 'deploy'];
    }

    /**
     * @return iterable<string[]>
     */
    public function after(): iterable
    {
        return [];
    }

    /**
     * @return iterable<string[]>
     */
    public function beforeRemove(): iterable
    {
        return [];
    }

    /**
     * @return iterable<string[]>
     */
    public function finish(): iterable
    {
        return [];
    }

    /**
     * @return iterable<string[]>
     */
    public function reload(): iterable
    {
        return [];
    }

    /**
     * @return iterable<string[]>
     */
    public function beforeSwitchVersion(): iterable
    {
        return [];
    }

    /**
     * @return iterable<string[]>
     */
    public function afterSwitchVersion(): iterable
    {
        return [];
    }
}
