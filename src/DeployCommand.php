<?php

namespace Avris\Deployer;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'deploy',
    description: 'Deploy the application',
    aliases: ['d']
)]
class DeployCommand extends Command
{
    public function __construct(private Deployer $deployer)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addArgument('branch', InputArgument::OPTIONAL, 'git branch to check out');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->deployer->deploy($input->getArgument('branch'));
        $output->write("\x07");

        return Command::SUCCESS;
    }
}
