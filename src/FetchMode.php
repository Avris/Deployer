<?php

namespace Avris\Deployer;

interface FetchMode
{
    /**
     * @return iterable<string[]>
     */
    public function fetch(Config $config, string $branch, string $releaseDir): iterable;
}
