<?php

namespace Avris\Deployer;

use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Process;

final class Deployer
{
    const TIMESTAMP_FORMAT = 'Y-m-d--H-i-s';
    const TIMESTAMP_FORMAT_LEGACY = 'YmdHis';

    private Filesystem $filesystem;
    private OutputInterface $output;
    private Config $config;
    private string $projectDir;
    private string $configFile;
    private string $currentLink;
    private string $releasesDir;
    private string $sharedDir;
    private bool $configured = false;

    public function __construct(string $projectDir, OutputInterface $output)
    {
        $this->projectDir = $projectDir;
        $this->output = $output;

        $this->filesystem = new Filesystem();

        $this->configFile = $this->projectDir . '/deploy.php';
        $this->currentLink = $this->projectDir . '/current';
        $this->releasesDir = $this->projectDir . '/release';
        $this->sharedDir = $this->projectDir . '/shared';

    }

    private function configure()
    {
        if ($this->configured) {
            return;
        }

        if (!$this->filesystem->exists($this->configFile)) {
            throw new \RuntimeException(sprintf(
                'Cannot deploy in directory %s - file deploy.php not found',
                $this->projectDir
            ));
        }

        $this->config = require_once $this->configFile;
        $this->configured = true;
    }

    public function deploy(?string $branch)
    {
        $this->configure();

        $start = microtime(true);

        $releaseDir = $this->releasesDir . '/' . date(self::TIMESTAMP_FORMAT);

        $this->step('Project directory: ' . $this->projectDir);

        $this->filesystem->mkdir($releaseDir);
        $this->filesystem->mkdir($this->sharedDir);

        $branch = $branch ?: $this->config->branch();
        sprintf('Fetching %s (%s)', $this->config->repositoryUrl(), $branch);
        foreach ($this->config->fetchMode()->fetch($this->config, $branch, $releaseDir) as $command) {
            $this->exec($command, $releaseDir);
        }

        foreach ($this->config->before() as $command) {
            $this->exec($command, $releaseDir);
        }

        $this->step('Linking shared files');
        foreach ($this->config->sharedFiles() as $from => $to) {
            if (is_int($from)) {
                $from = $to;
            }

            $this->debug(sprintf('Linking %s → %s', $from, $to));
            $this->filesystem->remove($releaseDir . '/' . $to);
            $this->filesystem->symlink($this->sharedDir . '/' . $from, $releaseDir . '/' . $to);
        }

        foreach ($this->config->build() as $command) {
            $this->exec($command, $releaseDir);
        }

        foreach ($this->config->after() as $command) {
            $this->exec($command, $releaseDir);
        }

        $this->step('Removing unnecessary files');
        foreach ($this->config->removeFiles() as $fileToRemove) {
            $this->debug(sprintf('Removing %s', $fileToRemove));
            $this->filesystem->remove($releaseDir . '/' . $fileToRemove);
        }

        $keepReleases = max(1, $this->config->keepReleases());
        $this->step(sprintf('Removing old releases (keep %s)', $keepReleases));
        $releasesToRemove = array_slice(
            $this->listOrderedReleases(),
            0,
            -$keepReleases
        );
        foreach ($releasesToRemove as $release) {
            $this->debug(sprintf('Removing release %s', $release));
            foreach ($this->config->beforeRemove() as $command) {
                $this->exec($command, $release);
            }
            $this->filesystem->remove($release);
        }

        $this->step('Linking current');
        $this->filesystem->symlink($releaseDir, $this->currentLink);

        foreach ($this->config->finish() as $command) {
            $this->exec($command, $this->currentLink);
        }

        foreach ($this->config->reload() as $command) {
            $this->exec($command, $this->currentLink);
        }

        $this->step('Successfully deployed');
        $stop = microtime(true);
        $duration = $stop - $start;
        $this->output->writeln(sprintf('Time taken: %.2fs', $duration) . ($duration >= 70 ? sprintf(' (%.2fm)', $duration / 60) : ''));
    }

    private function step(string $description)
    {
        $this->output->writeln('<info>→ ' . $description . '</info>');
    }

    private function debug(string $message)
    {
        $this->output->writeln($message, OutputInterface::VERBOSITY_VERBOSE);
    }

    private function exec(array $command, ?string $cwd = null)
    {
        $this->step('Exec: ' . join(' ', $command));

        $process = new Process($command, $cwd, null, null, null);
        $process->enableOutput();
        $process->mustRun(function (string $type, string $buffer) {
            if (!$this->output->isVerbose()) {
                return;
            }

            if ($type === Process::ERR) {
                $this->output->write('<error>' . $buffer . '</error>');
            } else {
                $this->output->write($buffer);
            }
        });
    }

    /**
     * @return string[]
     */
    private function listOrderedReleases(): array
    {
        $releasesWithCreationTimes = [];
        foreach (glob($this->releasesDir . '/*') as $release) {
            $releasesWithCreationTimes[$release] = filectime($release);
        }

        asort($releasesWithCreationTimes);

        return array_keys($releasesWithCreationTimes);
    }

    public function listVersions(): int
    {
        $this->configure();

        $activeIndex = -1;

        $active = $this->filesystem->readlink($this->currentLink);
        foreach ($this->listOrderedReleases() as $i => $dir) {
            $formatted = $this->formatVersion($dir);
            if ($dir === $active) {
                $this->output->writeln('<info> → ' . $formatted . '</info>');
                $activeIndex = $i;
            } else {
                $this->output->writeln(' – ' . $formatted);
            }
        }

        return $activeIndex;
    }

    public function switchVersion(int $newIndex): ?string
    {
        foreach ($this->listOrderedReleases() as $i => $dir) {
            if ($i === $newIndex) {
                foreach ($this->config->beforeSwitchVersion() as $command) {
                    $this->exec($command, $this->currentLink);
                }

                $this->filesystem->remove($this->currentLink);
                $this->filesystem->symlink($dir, $this->currentLink);

                foreach ($this->config->afterSwitchVersion() as $command) {
                    $this->exec($command, $this->currentLink);
                }

                foreach ($this->config->reload() as $command) {
                    $this->exec($command, $this->currentLink);
                }

                return $this->formatVersion($dir);
            }
        }

        return null;
    }

    private function formatVersion($path)
    {
        $dt = \DateTimeImmutable::createFromFormat(self::TIMESTAMP_FORMAT, basename($path))
            ?: \DateTimeImmutable::createFromFormat(self::TIMESTAMP_FORMAT_LEGACY, basename($path));

        return $dt->format('Y-m-d H:i:s');
    }

    public function init(string $repo): string
    {
        if ($this->filesystem->exists($this->configFile)) {
            throw new \RuntimeException(sprintf(
                'Deployer is already initialised in directory %s',
                $this->projectDir
            ));
        }

        $this->filesystem->mkdir($this->sharedDir);
        $this->filesystem->dumpFile($this->sharedDir . '/.env.local', 'APP_ENV=prod' . PHP_EOL);
        $this->filesystem->mkdir($this->sharedDir . '/var/log');

        $this->filesystem->dumpFile(
            $this->configFile,
            <<<PHP
<?php

return new class extends \Avris\Deployer\Config
{
    public function repositoryUrl(): string
    {
        return '$repo';
    }

    public function sharedFiles(): iterable
    {
        yield '.env.local';
        yield 'var/log';
    }

    public function removeFiles(): iterable
    {
        yield '.git';
        yield 'node_modules';
    }
};
PHP
        );

        return $this->projectDir;
    }
}
