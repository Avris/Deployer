<?php

namespace Avris\Deployer;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'version',
    description: 'Manage versions of the deployment',
)]
class VersionCommand extends Command
{
    public function __construct(private Deployer $deployer)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addArgument('action', InputArgument::OPTIONAL, 'list (default) | prev | next');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $activeIndex = $this->deployer->listVersions();
        $move = 0;

        switch ($input->getArgument('action') ?: 'list') {
            case 'list':
                break;
            case 'prev':
                $move = -1;
                break;
            case 'next':
                $move = 1;
                break;
            default:
                throw new InvalidArgumentException('Invalid action');
        }

        if (!$move) {
            return Command::SUCCESS;
        }

        $result = $this->deployer->switchVersion($activeIndex + $move);
        if ($result) {
            $output->writeln('');
            $output->writeln(sprintf('<info>New version: %s</info>', $result));
            $output->writeln('');
            $this->deployer->listVersions();
        } else {
            $output->writeln('');
            $output->writeln('<error>No version to switch to</error>');
            $output->writeln('');
        }

        $output->write("\x07");

        return Command::SUCCESS;
    }
}
