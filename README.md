# Avris Deployer

Simple deployment helper. This script:

 - clones the given repository and branch to a new directory (ie. without affecting live processes during the process)
 - symlinks files shared between deployments (like logs directory or `.env`)
 - executes deployment commands (defaulting to `make deploy`)
 - cleans up files that are unnecessary after a finished deployment (like `.git`, `node_modules`)
 - cleans up old releases (by default it keeps 3 most recent ones)
 - symlinks the latest release as the `current` directory
 - _(optionally)_ runs extra commands at specific stages of the deployment.

## Installation

    composer global require avris/deployer

In order to execute globally installed composer binaries, add the following to your `~/.bashrc` / `~/.zshrc`:

    export PATH="$(composer config -g home)/vendor/bin:$PATH"

## Usage

Create a folder to which you will deploy, and initialise Deployer there:

    mdkir project_name
    cd project_name
    deployer init <repo_url>

This command will create a directory `shared`. Put there all the files that are supposed to be shared between deployments.

It will also create `deploy.php` file. This file holds all the configuration of the deployment,
and it looks like this: 

    <?php
    
    return new class extends \Avris\Deployer\Config
    {
            public function repositoryUrl(): string
            {
                    return 'git@gitlab.com:Avris/OurSong.git';
            }
    
            public function sharedFiles(): iterable
            {
                    yield '.env';
                    yield 'keys';
                    yield 'server/.env.local';
                    yield 'server/var/geo.json';
                    yield 'server/var/log';
            }
    
            public function finish(): iterable
            {
                    yield ['sudo', 'service', 'php8.3-fpm', 'restart'];
            }
    };

By default, the command that's gonna be executed to build the release is `make deploy`.
To make it work like that, create a `Makefile` in your project's repository, eg.

    deploy:
        composer install --no-dev --optimize-autoloader
        bin/console doctrine:migration:migrate -n
        yarn
        yarn build


Methods that can be specified in the config file:
 - `repositoryUrl` (required)
 - `branch` (defaults to `main`)
 - `keepReleases` (defaults to `3`) – how many directories should be kept in the `release` folder
    (allows for quickly reverting to a previous version)
 - `sharedFiles` – a list of files or directories (relative paths) which should be shared between releases
   (by symlinking them from `shared` to each release)
 - `removeFiles` – a list of files or directories (relative paths) that should be removed after the build step is complete
 - `build` (defaults to `[make deploy]`) – a list of commands that build (compile etc.) the project
 - hooks – lists of commands to be executed at various stages:
   - `before` – runs at the very beginning of a deployment
   - `after` – runs right after `build`
   - `beforeRemove` – runs right before cleaning up an older release (in the cwd of that release)
   - `finish` – runs at the end of deployment
   - `beforeSwitchVersion` – runs right before switching to a different release
   - `afterSwitchVersion` – runs right after switching to a different release
   - `reload` – runs at the very and of both deployments and switches (intended eg. for reloading apache)
   
You can now execute a deployment: just run `deployer` (or `deployer <branch>` if you want to overwrite the default branch).

To switch between versions run `deployer version list|prev|next`.

## Copyright

* **Author:** Andrea Vos [(avris.it)](https://avris.it)
* **Licence:** [OQL](https://oql.avris.it)
